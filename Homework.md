---
title: "BTI7056-DB"
subtitle: "Homework 7"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
rule-color: 00c0c0
link-color: 00c0c0
...

# Aufgabe 1
> Realisieren Sie folgende Aufgaben in `university.db`.  
> Geben Sie je genau ein SQL statement an.

> 1. Erhöhe das Gehalt jedes CS-Instruktors um 10%.
```{.sql .numberLines}
UPDATE instructor SET
salary = salary * 1.1
WHERE dept_name = 'Comp. Sci.';
```

> 2. Lösche alle Kurse die nie angeboten wurden.
```{.sql .numberLines}
DELETE
FROM course
WHERE course_id NOT IN (
  SELECT course_id
  FROM SECTION
);
```

> 3. Stelle alle Studierenden mit mehr als 100 Credits als Instruktor im
> selben Department mit einem Gehalt von 30'000 ein.
```{.sql .numberLines}
INSERT INTO instructor (ID, name, dept_name, salary)
SELECT id,
       name,
       dept_name,
       30000 salary
FROM student
WHERE tot_cred > 100;
```

# Aufgabe 2
> Gegeben ist wieder das Schema von `publications.db` von der letzten
> Übung. Wieso ergeben die Abfragen nicht das erwartete Ergebnis?
> Was liefert sie stattdessen? Korrigieren Sie.

> 1. Titel die weniger als 20 Dollar kosten?
> ```{.sql}
> select title
> from titles
> where price < 20;
> ```

Diese Query scheint so zu funktionieren wie wir es erwarten würden.
`NULL`-Werte sind nicht inbegriffen, String-Werte auch nicht.

> 2. Autoren zusammen mit den Verlegern, bei denen sie publiziert haben?
> ```{.sql}
> select au_lname, pub_name
> from authors
> natural join titleauthor
> natural join titles
> natural join publishers;
> ```

Die Query liefert nur Autoren welche auch im gleichen Staat und in der
selben Stadt wohnen wie Publishers bei welche sie publiziert haben.
Weil `natural join` auf `au.state, pu.state` sowie `au.city, pu.city`
joined.

```{.sql .numberLines}
SELECT au.au_lname, pu.pub_name
FROM authors au
NATURAL JOIN titleauthor ta
NATURAL JOIN titles ti
JOIN publishers pu ON pu.pub_id = ti.pub_id;
```
\newpage

> 3. Alle Verleger, die höchstens zwei Psychologiebücher verlegt haben?
> ```{.sql}
> select pub_id, count(title_id) as numtitles
> from titles
> where type like 'psychology%'
> group by pub_id
> having numtitles <= 2;
> ```

Die Query liefert nur Publishers welche höchstens zwei
**aber auch mindestens ein** Psychologiebuch publiziert haben.
Wir ändern die originale Query nur Publishers zu liefern welche mehr
als zwei Psychologiebücher publiziert haben, und selektieren dann
all Publishers, welche *nicht* diese Kriterien erfüllen.

```{.sql .numberLines}
SELECT pub_id
FROM titles
WHERE pub_id NOT IN (
    SELECT pub_id
    FROM titles
    WHERE TYPE LIKE 'psychology%'
    GROUP BY pub_id
    HAVING count(title_id) > 2
)
GROUP BY pub_id;
```

# Aufgabe 3
> Gegeben sei folgendes Schema:
>
> person($\underline{\text{name}}$, street, city)  
> purchase($\underline{\text{name}}$, $\underline{\text{id}}$, number_of_items)  
> name $\mapsto$ person  
> id $\mapsto$ product  
> product($\underline{\text{id}}$, supplier_name, description, price)  
> supplier_name $\mapsto$ supplier  
> supplier($\underline{\text{name}}$, street, city)  
> 
> Beschreiben Sie umgangssprachlich das Resultat folgender Abfragen.

> 1. 
> ```{.sql}
> select name
> from person
> natural join purchase;
> ```

Liefert alle Personen, welche etwas gekauft haben.

> 2. 
> ```{.sql}
> select name
> from person
> natural join purchase
> natural join product
> natural join supplier;
> ```

Liefert alle Personen, welche ein Produkt gekauft haben,
von welchem Sie auch der Supplier sind.

> 3. 
> ```{.sql}
> select supplier_name, avg(price)
> from prduct
> group by supplier_name;
> ```

Liefert all Supplier, sowie den Durchschnittspreis ihrer jeweiligen Produkte.

\newpage

> 4. 
> ```{.sql}
> select supplier_name, avg(price)
> from product
> where id in (
>   select id
>   from purchase
> )
> group by supplier_name;
> ```

Liefert all Supplier, sowie den Durchschnittspreis ihrer jeweiligen Produkte,
welche auch wirklich verkauft wurden.

> 5. 
> ```{.sql}
> select sum(price * number_of_items)
> from person
> natural join purchase
> natural join product
> where name = "Hans Muster"
> ```

Liefert den Betrag welcher *Hans Muster* für Produkte bezahlt hat.

> 6. 
> ```{.sql}
> select id
> from product
> except
> select id
> from purchase
> where name = "Hans Muster";
> ```

Liefert die IDs aller Produkte die *Hans Muster* nicht gekauft hat.
